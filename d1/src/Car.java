public class Car {

    //An object is an idea of a real world object.
    //A class is the code that describes the object.
    //An instance is a tangible copy of an idea instantiated or created from a class.
    //attribute and methods
    private String make;
    private String brand;
    private int price;

    //Methods are functions of an object which allows us to perform certain tasks
    //void means that the fucntion does not return anything. Because in java, a function/methods' return dataType must be declared.


    public void start(){
        System.out.println("Vroom Vroom!");
    }
    //private = limit the access and ability to set a variable/method to its class.
    //setters are public methods which will allow us to set the value of an instance.
    //getters are public methods which will allow us to get the value of an attirbute of an instance

    //parameters in Java needs its dataType declared
    public void setMake(String makeParams){
        this.make = makeParams;
        //this keyword refers to the object where the constructor or setter is.

    }

    public String getMake(){
        return this.make;
    }

    /*
    *  create setters and getters for the brand and price props of our object.
    update the brand and price of car1 in the main clas using the setters
    print the brand and price of car1 in the main class using its getters.
     */

    public void setBrand(String makeParams){
        this.brand = makeParams;
    }
    public String getBrand(){
        return this.brand;
    }

    public void setPrice(int makeParams){
        this.price = makeParams;
    }
    public int getPrice(){
        return this.price;
    }

    // constructor is a method which allwos us to set the initial values of an instance
    //empty/default constructor - default contructor - allows us to create an instance with default initiALIZED values for our properties.

    public Car(){
        //empty or you can designate default values instead of getting them from parameters.
        //Java actually already creates one for us. however, empty/default structure contructor made just by Java allows Java to set its own default values. If you create your won, you can do it with your own default values.

        //parameterized constructor - allows us to initialize values to our attributes upon creation of the instance.

    }
    public Car(String make,String brand, int price){
        this.make = make;
        this.brand = brand;
        this.price = price;
    }

}
