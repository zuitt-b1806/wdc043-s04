
//This one is called class
public class Main {
    //This one is probs called object
    public String sample = "Sample";

    public static void main(String[] args) {
        //This one is called Intance
//        Main newSample = new Main();
//        System.out.println(newSample.sample);
//        Car car1 = new Car();

//        car1.make = "Innova";
//        car1.brand = "Toyota";
//        car1.price = 1000000;
//
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);

        //each instance of a class should be independent from one another especially their properties. However, since coming fom the main class, they may have the same methods.

        Car car2 = new Car();

//        car2.make = "Veyron";
//        car2.brand = "Bugatti";
//        car2.price = 2000000;
//
//        System.out.println(car2.make);
//        System.out.println(car2.brand);
//        System.out.println(car2.price);

        Car car3 = new Car();
//
//        car3.make = "Adventure";
//        car3.brand = "Toyota";
//        car3.price = 500000;
//
//        System.out.println(car3.make);
//        System.out.println(car3.brand);
//        System.out.println(car3.price);

        /*

            create a new instance of the car class and save it in a variable called car3. Then access the properties of the instance and update its values.


        */

//        car3.start();
//        car1.setMake("Veyron");
//        System.out.println(car1.getMake());
//
//        car1.setBrand("Bugatti");
//        System.out.println(car1.getBrand());
//
//        car1.setPrice(1000000);
//        System.out.println(car1.getPrice());


        // when creating a new instance of a class:
        // new keyword =  new allows us to create a new instance
        //Car() = constructor of our class - without arguments, is called default constructor which Java can defnie for us.
//
//
//        Car car4 = new Car();
//
//        Car car5 = new Car("Corolla", "Toyota", 1000000);
//
//        System.out.println(car5.getMake());
//        System.out.println(car5.getBrand());
//        System.out.println(car5.getPrice());
//

        Driver driver1 = new Driver("LeBron", 30, "Los Angeles");

        Driver driver2 = new Driver();

        System.out.println("Details of driver:");
        System.out.println(driver1.getName());
        System.out.println(driver1.getAge());
        System.out.println(driver1.getAddress());

        System.out.println("Details of driver (default values): ");
        System.out.println(driver2.getName());
        System.out.println(driver2.getAge());
        System.out.println(driver2.getAddress());

        System.out.println("Default driver(change name details):");
        driver1.setName("Kyrie");
        System.out.println(driver1.getName());
    }
}