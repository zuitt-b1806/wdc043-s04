public class Driver {
    /*
    * create a class called drive with the ff attribs:
    * name - string
    * age - int
    * address - string
    *
    * add a default and parametrized constructor for the class.
    * add getters and setters for the name, age and address attrib
    * in the main method of the main class, create a new instafnce of th edrive*/

    private String name;

    private int age;

    private String address;

//setters
    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }
//getters


    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public String getAddress() {
        return this.address;
    }



//constructors
    public Driver(){

    }
    public Driver(String name, int age, String address){
        this.name = name;
        this.age = age;
        this.address = address;
    }

}
